package beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import modelos.clientes.Cliente;
import repos.ClienteRepo;
 
@ManagedBean
@ViewScoped
public class PasswordView {
     
    private String password4; 
    private String username;
    private String endereco;
    
    ClienteRepo cr = ClienteRepo.getInstance();
    
    public String getPassword4() {
        return password4;
    }
 
    public void setPassword4(String password4) {
        this.password4 = password4;
    }

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	
	public String cadastrar() {
		Cliente c = new Cliente();
		c.setLogin(username);
		c.setSenha(password4);
		c.setEndereco(endereco);
		cr.addCliente(c);
		return "/index.xhtml";
	}
 
}
