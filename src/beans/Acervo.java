package beans;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import modelos.Carrinho;
import modelos.clientes.Cliente;
import modelos.exemplar.BluRay;
import modelos.exemplar.DVD;
import modelos.exemplar.Profissional;
import modelos.exemplar.Titulo;
import modelos.locacoes.Locacao;
import repos.ClienteRepo;
import repos.LocacoesRepo;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

@ManagedBean (name = "acervoFilmes")
@SessionScoped
public class Acervo {
	private String termo;
	private List<Titulo> acervo = new ArrayList<Titulo>();
	Carrinho carrinho;
	private Date dataDevolucao;
	private Date dataAluguel;
	ClienteRepo cr = ClienteRepo.getInstance();

	public String getSubTotal() {
		return carrinho.valorSubtotal();
	}

	public BigDecimal getSubTotalReal() {
		return carrinho.valorSubtotalReal();
	}

	public String getTermo() {
		return termo;
	}

	public void setTermo(String termo) {
		this.termo = termo;
	}

	public List<Titulo> getResultados() {
		return acervo;
	}

	public void setResultados(List<Titulo> acervo) {
		this.acervo = acervo;
	}

	public Acervo() {
		carrinho = new Carrinho();
		loadFilmeList();
	}

	public List<Titulo> getCarrinhoLista() {
		return carrinho.getLista();
	}

	public void addCarrinhoListaDvd(Titulo titulo) {
		titulo.setMidia("dvd");
		carrinho.addTitulo(titulo);
	}

	public void addCarrinhoListaBluRay(Titulo titulo) {
		titulo.setMidia("bluray");
		carrinho.addTitulo(titulo);
	}

	public void procurar() {
		acervo.removeAll(acervo);
		loadFilmeList();

		List<Titulo> foundResults = new ArrayList<Titulo>();

		if (termo != null) {
			for (Titulo titulo : acervo) {
				if (titulo.getNome().toLowerCase().contains(termo.toLowerCase()) 
						|| titulo.getNomeOriginal().toLowerCase().contains(termo.toLowerCase())
						|| titulo.getCategoria().toLowerCase().contains(termo.toLowerCase())
						|| titulo.getAno().toLowerCase().contains(termo.toLowerCase())
						|| titulo.getDiretor().toLowerCase().contains(termo.toLowerCase())
						) {foundResults.add(titulo);}
				else {
					for (Profissional prof : titulo.getProfissionais()) {
						if (prof.getNome().toLowerCase().contains(termo.toLowerCase()) || prof.getPais().toLowerCase().contains(termo.toLowerCase())) {
							foundResults.add(titulo);
						}
					}
				}
			}
			acervo = foundResults;
		}
	}

	@SuppressWarnings("deprecation")
	public void loadFilmeList() {
		List<Profissional> profs = new ArrayList<Profissional>();
		profs.add(new Profissional("Autor 1", new Date(1922,12,25),"USA"));
		acervo.add(new Titulo(1,
				"Cidad�o Kane", 
				"Citizen Kane",
				"Orson Welles", 
				profs, "1976", 
				"120min", 
				null, 
				"A��o", 
				"R$ 17,45",
				"22/12/2002",
				new DVD(5),
				new BluRay(3),
				"Pixar Studios",
				null,
				17.45));

		List<Profissional> profs2 = new ArrayList<Profissional>();
		profs2.add(new Profissional("Daphne du Maurier", new Date(1972,12,22),"USA"));
		acervo.add(new Titulo(2,
				"Hotel Jamaica", 
				"Jamaica Inn ",
				"Alfred Hitchcock", 
				profs2, 
				"1998", 
				"134min", 
				null, 
				"Suspense", 
				"R$ 15,45", 
				"22/12/2014",
				new DVD(8),
				new BluRay(12),
				"MGM",
				null,
				15.45));
		List<Profissional> profs3 = new ArrayList<Profissional>();
		profs3.add(new Profissional("Sidney Gilliat", new Date(1933,11,12),"USA"));
		acervo.add(new Titulo(3,
				"A Regra do Jogo", 
				"La Regle du Jeu",
				"Jean Renoir",
				profs3, 
				"2001", 
				"174min", 
				null, 
				"Romance", 
				"R$ 14,45",
				"22/12/2003",
				new DVD(13), 
				new BluRay(7),
				"DreamWorks",
				null,
				14.45));

		Cliente c = new Cliente();
		c.setLogin("raphael");
		c.setSenha("123");
		c.setEndereco("Rua Idume, 196");
		cr.addCliente(c);
	}

	public List<Titulo> getAcervo() {
		return acervo;
	}
	
	public Titulo getTituloById(int i) {
		for (Titulo titulo : Acervo.getInstance().getAcervo()) {
			if (titulo.getId() == i) {
				return titulo;
			}
		}
		return null;
	}

	public void setAcervo(List<Titulo> acervo) {
		this.acervo = acervo;
	}
	
	public void onDateSelect(SelectEvent event) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Date Selected", format.format(event.getObject())));
    }
     
    public void click(List<Titulo> lista, UserLoginView ulv) {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        
        requestContext.update("form:display");
        requestContext.execute("PF('dlg').show()");
        
        carrinho.createLocacao(lista, dataDevolucao, ulv);
    }
    
    public List<Locacao> getLocacoes() {
    	return LocacoesRepo.getInstance().getLocacoes();
    }
    
    public static Acervo instance;
    public static Acervo getInstance() {
    	if (instance == null) {
    		instance = new Acervo();
    	}
    	return instance;
    }

	public Date getDataDevolucao() {
		return dataDevolucao;
	}

	public void setDataDevolucao(Date dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}

	public Date getDataAluguel() {
		return dataAluguel;
	}

	public void setDataAluguel(Date dataAluguel) {
		this.dataAluguel = dataAluguel;
	}
    
}
