package beans;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.context.RequestContext;

import modelos.clientes.Cliente;
import modelos.exemplar.Titulo;
import modelos.locacoes.Locacao;
import repos.ClienteRepo;
import repos.LocacoesRepo;
 
@ManagedBean
@SessionScoped
public class UserLoginView {
     
    private String username;
    private String password;
    private boolean loggedIn = false;
	private Cliente c;
	public boolean loginButton = false;
	boolean duplicata = false;
	FacesMessage message = null;
	
	private String dupedItemTitulo = "";
    private String dupedItemData = "";
	private String dupedItemMessage;
 
    public String getUsername() {
        return username;
    }
 
    public void setUsername(String username) {
        this.username = username;
    }
 
    public String getPassword() {
        return password;
    }
 
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String cadastrar() {
    	return ("/cadastro.xhtml");
    }
   
    public String confirmarDupeRedirect(List<Titulo> listacarrinho, String username) {
//    	Date data = null;
    	for (Locacao locacao : LocacoesRepo.getInstance().getLocacoes()) {
			if (locacao.getLocador().getLogin().equalsIgnoreCase(username)) {
				for (Titulo dupeTitulo : locacao.getItens()) {
					for (Titulo tituloNew : listacarrinho ) {
						if (dupeTitulo.getId() == tituloNew.getId()) {
							duplicata = true;
							dupedItemTitulo = dupeTitulo.getNome();
							dupedItemData = locacao.getDataAluguel().toString();
//							if (data == null || data.after(new Date(locacao.getData()))) {
								dupedItemMessage = "Aten��o! Voc� j� possui um hist�rico de alugueis com este t�tulo: " + dupedItemTitulo
										+ " alugado na data: " + dupedItemData;
//							}
//							new Date(dupedItemData);
							
						}
					}
				}
			}
		}
    	if (loggedIn) 
    		return "/confirmar.xhtml";
    	return null;
    }
    
    @SuppressWarnings("finally")
	public String login(ActionEvent event) {
        RequestContext context = RequestContext.getCurrentInstance();
        message = null;
        
        ClienteRepo cr = ClienteRepo.getInstance();
        c = cr.logar(username, password);
//        if (!duplicata) {
        	if(c != null) {
                loggedIn = true;
                message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Welcome", username);
//            } else {
//                loggedIn = false;
//                message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Loggin Error", "Invalid credentials");
//            }
        } else {
        	message = new FacesMessage(FacesMessage.SEVERITY_INFO, "ATEN��O", "Voc� j� tem hist�rico de alugu�is com esse t�tulo.");
        }
        
         
        FacesContext.getCurrentInstance().addMessage(null, message);
        context.addCallbackParam("loggedIn", loggedIn);
        
        if (event.getComponent().getId().equalsIgnoreCase("loginButton")) {
			try {
				ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
				ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				return "/index.html";
			}
        } else return null;
    }
    
    /*private static UserLoginView instance;
    public static UserLoginView getInstance() {
    	if (instance == null) {
    		instance = new UserLoginView();
    	}
    	return instance;
    }*/
    
	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	
	@SuppressWarnings("finally")
	public String logout() {
	    try {
	    	loggedIn = false;
	    	ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
			ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			return "/index.html?faces-redirect=true";
		}
	}

	public boolean isLoginButton() {
		return loginButton;
	}

	public void setLoginButton(boolean loginButton) {
		this.loginButton = loginButton;
	}

	public boolean isDuplicata() {
		return duplicata;
	}

	public void setDuplicata(boolean duplicata) {
		this.duplicata = duplicata;
	}

	public String getDupedItemTitulo() {
		return dupedItemTitulo;
	}

	public void setDupedItemTitulo(String dupedItemTitulo) {
		this.dupedItemTitulo = dupedItemTitulo;
	}

	public String getDupedItemData() {
		return dupedItemData;
	}

	public void setDupedItemData(String dupedItemData) {
		this.dupedItemData = dupedItemData;
	}

	public String getDupedItemMessage() {
		return dupedItemMessage;
	}

	public void setDupedItemMessage(String dupedItemMessage) {
		this.dupedItemMessage = dupedItemMessage;
	}
	
}
