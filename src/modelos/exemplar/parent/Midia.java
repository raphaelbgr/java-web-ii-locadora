package modelos.exemplar.parent;

public abstract class Midia {

	abstract public int getQuantidade();

	abstract public void setQuantidade(int quantidade);
	
}
