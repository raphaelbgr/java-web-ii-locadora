package modelos.exemplar;

import java.util.Date;

public class Profissional {

	private String nome;
	private Date anoNasc;
	private String pais;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Date getAnoNasc() {
		return anoNasc;
	}
	public void setAnoNasc(Date anoNasc) {
		this.anoNasc = anoNasc;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	public Profissional() {
	}
	
	public Profissional(String nome, Date anoNasc, String pais) {
		this.nome = nome;
		this.anoNasc = anoNasc;
		this.pais = pais;
	}
	
}
