package modelos.exemplar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import modelos.clientes.Cliente;
import modelos.exemplar.parent.Midia;
import beans.Acervo;

@ManagedBean (name = "Filme")
@ViewScoped
public class Titulo {
	private String nome;
	private String nomeOriginal;
	private String diretor;
	private String ano;
	private String duracao;
	private String categoria;
	private String valorUnit;
	private String estudio;
	private String media_type;
	
	private long timeCreatedHash = 0L;
	
	private Cliente c;
	
	private int id;
	
	private String dataAquisicao;
	
	private List<Profissional> profissionais;
	private List<Midia> midias;
	private Double valorUnitReal;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNomeOriginal() {
		return nomeOriginal;
	}
	public void setNomeOriginal(String nomeOriginal) {
		this.nomeOriginal = nomeOriginal;
	}
	public String getDiretor() {
		return diretor;
	}
	public void setDiretor(String diretor) {
		this.diretor = diretor;
	}
	public List<Midia> getLista() {
		return midias;
	}
	public void addMidia(Midia midia) {
		this.midias.add(midia);
	}
	private void setProfissionais(List<Profissional> profissionais) {
		this.profissionais = profissionais;
	}
	public List<Profissional> getProfissionais() {
		return profissionais;
	}
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}
	public String getDuracao() {
		return duracao;
	}
	public void setDuracao(String duracao) {
		this.duracao = duracao;
	}
	public List<Midia> getMidias() {
		return midias;
	}
	public void setMidias(List<Midia> midias) {
		this.midias = midias;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getQtdtotal() {
		return dispoDVD(this) + " DVD(s) e " + dispoBluRay(this) + " BluRay(s)";
	}
	
	public boolean debitQtd(Titulo titulo) {
		if (titulo.getMidia().equalsIgnoreCase("dvd")) {
			if (Integer.valueOf(dispoDVD(this)) > 0) {
				Acervo.getInstance().getTituloById(titulo.getId()).getMidias().get(0).setQuantidade(Acervo.getInstance().getTituloById(titulo.getId()).getMidias().get(0).getQuantidade() - 1);
				return true;
			}
		} else if (titulo.getMidia().equalsIgnoreCase("bluray")) {
			if (Integer.valueOf(dispoBluRay(this)) > 0) {
				Acervo.getInstance().getTituloById(titulo.getId()).getMidias().get(1).setQuantidade(Acervo.getInstance().getTituloById(titulo.getId()).getMidias().get(1).getQuantidade() - 1);
				return true;
			}
		}
		return false;
	}
	
	public String dispoDVD(Titulo titulo) {
		for (Titulo titacervocmp : Acervo.getInstance().getAcervo()) {
			if (titulo.getId() == titacervocmp.getId()) {
				for (Midia media : getMidias()) {
					if (media instanceof DVD) {
						return Integer.toString(titacervocmp.getMidias().get(0).getQuantidade());
					}
				}
			}
		}
		return null;
	}
	
	public String dispoBluRay(Titulo titulo) {
		for (Titulo titacervocmp : Acervo.getInstance().getAcervo()) {
			if (titulo.getId() == titacervocmp.getId()) {
				for (Midia media : getMidias()) {
					if (media instanceof BluRay) {
						return Integer.toString(titacervocmp.getMidias().get(1).getQuantidade());
					}
				}
			}
		}
		return null;
	}
	public String getValorUnit() {
		return valorUnit;
	}
	public Double getValorUnitReal() {
		return valorUnitReal;
	}
	public void setValorUnitReal(Double valorUnitReal) {
		this.valorUnitReal = valorUnitReal;
	}
	public void setValorUnit(String valorUnit) {
		this.valorUnit = valorUnit;
	}
	public String getDataAquisicao() {
		return dataAquisicao.toString();
	}
	public void setDataAquisicao(String dataAquisicao) {
		this.dataAquisicao = dataAquisicao;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEstudio() {
		return estudio;
	}
	public void setEstudio(String estudio) {
		this.estudio = estudio;
	}
	
	// CONSTRUCTORS
	public Titulo(
			int id,
			String nome,
			String nomeOriginal,
			String diretor,
			List<Profissional> profissionais,
			String ano,
			String duracao,
			List<Midia> midias,
			String categoria,
			String valorUnit,
			String dataAquisicao,
			DVD dvd,
			BluRay bluray,
			String estudio,
			String media_type,
			Double valorUnitRDouble) {
		
		this.midias = new ArrayList<Midia>();
		
		setId(id);
		setNome(nome);
		setNomeOriginal(nomeOriginal);
		setDiretor(diretor);
		setAno(ano);
		setDuracao(duracao);
		addMidia(dvd);
		addMidia(bluray);
		setCategoria(categoria);
		setValorUnit(valorUnit);
		setDataAquisicao(dataAquisicao);
		setEstudio(estudio);
		setMidia(media_type);
		setValorUnitReal(valorUnitRDouble);
		
		List<Profissional> list = new ArrayList<Profissional>();
		for (Profissional prof : profissionais) {
			list.add(prof);
		}
		setProfissionais(list);
	}
	public String getMidia() {
		return media_type;
	}
	public void setMidia(String media_type) {
		this.media_type = media_type;
	}
	public long getTimeCreatedHash() {
		return timeCreatedHash;
	}
	public void createTimeHash() {
		this.timeCreatedHash = Calendar.getInstance().getTimeInMillis();
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ano == null) ? 0 : ano.hashCode());
		result = prime * result + ((categoria == null) ? 0 : categoria.hashCode());
		result = prime * result + ((dataAquisicao == null) ? 0 : dataAquisicao.hashCode());
		result = prime * result + ((diretor == null) ? 0 : diretor.hashCode());
		result = prime * result + ((duracao == null) ? 0 : duracao.hashCode());
		result = prime * result + ((estudio == null) ? 0 : estudio.hashCode());
		result = prime * result + id;
		result = prime * result + ((media_type == null) ? 0 : media_type.hashCode());
		result = prime * result + ((midias == null) ? 0 : midias.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((nomeOriginal == null) ? 0 : nomeOriginal.hashCode());
		result = prime * result + ((profissionais == null) ? 0 : profissionais.hashCode());
		result = prime * result + (int) (timeCreatedHash ^ (timeCreatedHash >>> 32));
		result = prime * result + ((valorUnit == null) ? 0 : valorUnit.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Titulo other = (Titulo) obj;
		if (ano == null) {
			if (other.ano != null)
				return false;
		} else if (!ano.equals(other.ano))
			return false;
		if (categoria == null) {
			if (other.categoria != null)
				return false;
		} else if (!categoria.equals(other.categoria))
			return false;
		if (dataAquisicao == null) {
			if (other.dataAquisicao != null)
				return false;
		} else if (!dataAquisicao.equals(other.dataAquisicao))
			return false;
		if (diretor == null) {
			if (other.diretor != null)
				return false;
		} else if (!diretor.equals(other.diretor))
			return false;
		if (duracao == null) {
			if (other.duracao != null)
				return false;
		} else if (!duracao.equals(other.duracao))
			return false;
		if (estudio == null) {
			if (other.estudio != null)
				return false;
		} else if (!estudio.equals(other.estudio))
			return false;
		if (id != other.id)
			return false;
		if (media_type == null) {
			if (other.media_type != null)
				return false;
		} else if (!media_type.equals(other.media_type))
			return false;
		if (midias == null) {
			if (other.midias != null)
				return false;
		} else if (!midias.equals(other.midias))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (nomeOriginal == null) {
			if (other.nomeOriginal != null)
				return false;
		} else if (!nomeOriginal.equals(other.nomeOriginal))
			return false;
		if (profissionais == null) {
			if (other.profissionais != null)
				return false;
		} else if (!profissionais.equals(other.profissionais))
			return false;
		if (timeCreatedHash != other.timeCreatedHash)
			return false;
		if (valorUnit == null) {
			if (other.valorUnit != null)
				return false;
		} else if (!valorUnit.equals(other.valorUnit))
			return false;
		return true;
	}
	public Titulo cloneFilme(Titulo titulo) {
		Titulo filme = new Titulo(
				titulo.getId(),
				titulo.getNome(),
				titulo.getNomeOriginal(),
				titulo.getDiretor(),
				titulo.getProfissionais(),
				titulo.getAno(),
				titulo.getDuracao(),
				titulo.getMidias(),
				titulo.getCategoria(),
				titulo.getValorUnit(),
				titulo.getDataAquisicao(),
				new DVD(titulo.getMidias().get(0).getQuantidade()),
				new BluRay(titulo.getMidias().get(0).getQuantidade()),
				titulo.getEstudio(),
				titulo.getMidia(),
				titulo.getValorUnitReal());
		return filme;
	}
	
	@Override
	public String toString() {
		return this.nomeOriginal + ", " + ano + ", " + this.duracao + ", " + ano + " (" + media_type.toUpperCase() + ")";
	}
	public Cliente getC() {
		return c;
	}
	public void setC(Cliente c) {
		this.c = c;
	}
	
}
