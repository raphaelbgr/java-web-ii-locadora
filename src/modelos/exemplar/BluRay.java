package modelos.exemplar;

import modelos.exemplar.parent.Midia;


public class BluRay extends Midia {

	private int quantidade;
	
	@Override
	public int getQuantidade() {
		return quantidade;
	}

	@Override
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	@Override
	public String toString() {
		return "BluRay";
	}
	
	public BluRay(int i) {
		this.quantidade = i;
	}
}
