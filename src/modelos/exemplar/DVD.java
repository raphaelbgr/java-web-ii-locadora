package modelos.exemplar;

import modelos.exemplar.parent.Midia;


public class DVD extends Midia {

	private int quantidade;

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	
	@Override
	public String toString() {
		return "DVD";
	}
	
	public DVD(int i) {
		this.quantidade = i;
	}
}
