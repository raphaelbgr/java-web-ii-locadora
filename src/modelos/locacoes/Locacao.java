package modelos.locacoes;

import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;

import modelos.Carrinho;
import modelos.clientes.Cliente;
import modelos.exemplar.Titulo;

@ManagedBean
public class Locacao {
	private String data;
	private String status;
	private Cliente locador;
	private List<Titulo> itens;
	private int id;
	private Date dataDevolucao;
	private Date dataAluguel;
	
	public static boolean executeTransaction(Carrinho carrinho) {
		for (Titulo filme : carrinho.getLista()) {
			if (!(filme.getMidias().get(0).getQuantidade() >= carrinho.getQuantidadeNoAcervoDvd(filme))) { // indice 0 = dvd
				return false;
			}
			if (!(filme.getMidias().get(1).getQuantidade() >= carrinho.getQuantidadeNoAcervoBluRay(filme))) {
				return false;
			}
		}
		for (Titulo filme : carrinho.getLista()) {
			filme.getMidias().get(0).setQuantidade(filme.getMidias().get(0).getQuantidade() -1); // indice 0 = dvd
		}
		return true;
	}
	
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Cliente getLocador() {
		return locador;
	}
	public void setLocador(Cliente locador) {
		this.locador = locador;
	}
	public List<Titulo> getItens() {
		return itens;
	}
	public void setItens(List<Titulo> itens) {
		this.itens = itens;
	}

	public void setId(int i) {
		this.id = i;
	}

	public int getId() {
		return this.id;
	}

	public void getDataDevolucao(Date dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}

	public Date getDataDevolucao() {
		return dataDevolucao;
	}

	public Date getDataAluguel() {
		return dataAluguel;
	}

	public void setDataAluguel(Date dataAluguel) {
		this.dataAluguel = dataAluguel;
	}
	
}
