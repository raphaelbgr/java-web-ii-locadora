package modelos;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import repos.SugestaoRepo;

@ManagedBean (name = "sugestaoBean")
@SessionScoped
public class Sugestao {
	
	private String sugestao;
	
	public String addSugestao() {
		if (sugestao != null){
			SugestaoRepo.sugestoes.add(sugestao);
		}
		return "/index.xhtml";
	}

	public String getSugestao() {
		return sugestao;
	}

	public void setSugestao(String sugestao) {
		this.sugestao = sugestao;
	}
	
	public List<String> getSugestoes() {
		return SugestaoRepo.sugestoes;
	}
	
}
