package modelos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import beans.UserLoginView;
import modelos.exemplar.Titulo;
import modelos.locacoes.Locacao;
import repos.ClienteRepo;
import repos.LocacoesRepo;

@SessionScoped
@ManagedBean	
public class Carrinho {
	private List<Titulo> lista = new ArrayList<Titulo>();
	private Date dateEntrega;
	private Date dateAluguel;

	public List<Titulo> getLista() {
		return lista;
	}
	
	public String valorSubtotal() {
		double i = 0d;
		for (Titulo titulo : lista) {
			i = i + Double.valueOf(titulo.getValorUnit());
		}
		return String.valueOf(i);
	}
	
	public BigDecimal valorSubtotalReal() {
		double i = 0d;
		for (Titulo titulo : lista) {
			i = i + titulo.getValorUnitReal();
		}
		return new BigDecimal(i);
	}
	
	public int getQuantidadeNoAcervoDvd(Titulo filme) {
		int i = 0;
		for (Titulo titulo : getLista()) {
			if (titulo.getId() == filme.getId() && titulo.getMidia().equalsIgnoreCase("dvd") && filme.getMidia().equalsIgnoreCase("dvd")) {
				i++;
			}
		}
		return i;
	}
	
	public int getQuantidadeNoAcervoBluRay(Titulo filme) {
		int i = 0;
		for (Titulo titulo : getLista()) {
			if (titulo.getId() == filme.getId() && titulo.getMidia().equalsIgnoreCase("bluray") && filme.getMidia().equalsIgnoreCase("bluray")) {
				i++;
			}
		}
		return i;
	}
	
	public void addTitulo(Titulo titulo) {
		Titulo filme = titulo.cloneFilme(titulo);
		filme.createTimeHash();
		this.lista.add(filme);
	}
	
	public boolean createLocacao(List <Titulo> itens, Date dataDevolucao, UserLoginView ulv) {
		Locacao locacao = new Locacao();
		List<Titulo> itens2 = new ArrayList<Titulo>();
		itens2.addAll(itens);
		locacao.setItens(itens2);
		locacao.getDataDevolucao(dataDevolucao);
		locacao.setId(locacao.getId());
		locacao.setDataAluguel(Calendar.getInstance().getTime());
		locacao.setLocador(ClienteRepo.getInstance().getClienteByLogin(ulv.getUsername()));
		
		if (LocacoesRepo.getInstance().addLocacao(locacao)) {
			lista.clear();
			ulv.setDuplicata(false);
			return true;
		}
		else {
			return false;
		}
	}
	
	public Date getDate1() {
		return dateEntrega;
	}

	public void setDate1(Date date1) {
		this.dateEntrega = date1;
	}

	public Date getDateAluguel() {
		return dateAluguel;
	}

	public void setDateAluguel(Date dateAluguel) {
		this.dateAluguel = dateAluguel;
	}
    
}
