package repos;

import java.util.ArrayList;
import java.util.List;

import modelos.clientes.Cliente;

public class ClienteRepo {
	
	private static List<Cliente> clientes = new ArrayList<Cliente>();

	public List<Cliente> getPessoas() {
		return clientes;
	}

	public void setCliente(List<Cliente> clientes) {
		ClienteRepo.clientes = clientes;
	}
	
	public void addCliente(Cliente c) {
		clientes.add(c);
	}
	
	public Cliente logar(String login, String senha) {
		for (Cliente cliente : clientes) {
			if (cliente.getLogin() != null && cliente.getLogin().equalsIgnoreCase(login)) {
				if (cliente.getSenha() != null && cliente.getSenha().equalsIgnoreCase(senha)) {
					return cliente;
				}
			}
		}
		return null;
	}
	
	public Cliente getClienteByLogin(String login) {
    	for (Cliente c : ClienteRepo. getInstance().getPessoas()) {
			if (c.getLogin().equalsIgnoreCase(c.getLogin())) {
				return c;
			}
		}
    	return null;
    }
	
	private static ClienteRepo instance;
	public static ClienteRepo getInstance() {
		if (instance == null) {
			instance = new ClienteRepo();
		}
		return instance;
	}
}
