package repos;

import java.util.ArrayList;
import java.util.List;

import modelos.clientes.Cliente;
import modelos.exemplar.Titulo;
import modelos.locacoes.Locacao;
import beans.Acervo;

public class LocacoesRepo {
	List<Locacao> locacoes = new ArrayList<Locacao>();
	private Cliente c;
	
	public boolean addLocacao(Locacao locacao) {
		// Verifica por titulos alugados do mesmo nome, pelo cliente
		/*for (Locacao locacaodb : getLocacoes()) {
			for (Titulo tituloloc : locacaodb.getItens()) {
				for (Titulo titulocliente : locacao.getItens()) {
					if (locacao.getLocador().getLogin().equalsIgnoreCase(locacaodb.getLocador().getLogin())) {
						return false;
					}
				}
			}
		}*/
		locacao.setId(locacoes.size() + 1);
		locacao.setStatus("Em aluguel");
		for (Titulo titulo : locacao.getItens()) {
			for (Titulo titacervo : Acervo.getInstance().getAcervo()) {
				if (titulo.getId() == titacervo.getId()) {
					if (titulo.getMidia().equalsIgnoreCase("dvd")) {
						titacervo.debitQtd(titulo);
					} else if (titulo.getMidia().equalsIgnoreCase("bluray")) {
						titacervo.debitQtd(titulo);
					}
				}
			}
		}
		locacoes.add(locacao);
		return true;
	}
	
	public Locacao searchLocacao(int id) {
		for (Locacao locacao : locacoes) {
			if (locacao.getId() == id) {
				return locacao;
			}
		}
		return null;
	}
	
	private static LocacoesRepo locacoesrepo;
	public static LocacoesRepo getInstance() {
		if (locacoesrepo == null) {
			locacoesrepo = new LocacoesRepo();
		}
		return locacoesrepo;
	}

	public List<Locacao> getLocacoes() {
		return locacoes;
	}

	public Cliente getC() {
		return c;
	}

	public void setC(Cliente c) {
		this.c = c;
	}
}
